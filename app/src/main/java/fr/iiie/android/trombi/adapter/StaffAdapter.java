package fr.iiie.android.trombi.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import fr.iiie.android.trombi.R;
import fr.iiie.android.trombi.rest.model.Staff;

/**
 * Created by francois.bertet on 2/5/2015.
 */
public class StaffAdapter extends BaseAdapter {

    private final List<Staff> dataList;
    private final LayoutInflater layoutInflater;

    public StaffAdapter(Context context) {
        this.layoutInflater = LayoutInflater.from(context);
        this.dataList = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Staff getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Staff staff = dataList.get(position);
        View rowView;
        ViewHolder viewHolder;

        if (convertView == null)
        {
            rowView = layoutInflater.inflate(R.layout.list_item_staff, parent, false);
            viewHolder = new ViewHolder(rowView);
            rowView.setTag(viewHolder);
        }
        else
        {
            rowView = convertView;
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.firstNameTextView.setText(staff.getFirstname());
        viewHolder.lastNameTextView.setText(staff.getLastname());

        final int drawableId = layoutInflater.getContext().getResources().getIdentifier(
                "photo_" + Integer.toString(staff.getId()),
                "drawable", layoutInflater.getContext().getPackageName());
        viewHolder.iconImageView.setImageResource(R.drawable.ic_launcher);

        return rowView;
    }

    class ViewHolder
    {
        @InjectView(R.id.list_item_staff_textview_firstname)
        TextView firstNameTextView;
        @InjectView(R.id.list_item_staff_textview_lastname)
        TextView lastNameTextView;
        @InjectView(R.id.list_item_staff_imageview_icon)
        ImageView iconImageView;

        ViewHolder(View v) {
            ButterKnife.inject(this, v);
        }
    }

    public void updateList(final ArrayList<Staff> staffArrayList)
    {
        dataList.clear();
        addaToList(staffArrayList);
    }

    public void addaToList(final ArrayList<Staff> staffArrayList)
    {
        dataList.addAll(staffArrayList);
        this.notifyDataSetChanged();
    }
}
