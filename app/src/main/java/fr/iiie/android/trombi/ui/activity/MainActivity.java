package fr.iiie.android.trombi.ui.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.activeandroid.ActiveAndroid;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;
import fr.iiie.android.trombi.R;
import fr.iiie.android.trombi.app.App;
import fr.iiie.android.trombi.bus.GotoIngButtonClickedEvent;
import fr.iiie.android.trombi.bus.IngListItemClickedEvent;
import fr.iiie.android.trombi.ui.fragment.IngDetailsFragment;
import fr.iiie.android.trombi.ui.fragment.IngListFragment;
import fr.iiie.android.trombi.ui.fragment.LabFragment;
import fr.iiie.android.trombi.ui.fragment.StaffFragment;


public class MainActivity extends ActionBarActivity {

    @InjectView(R.id.navigation_drawer_listview)
    ListView navigationDrawerListView;
    @InjectView(R.id.activity_main_drawerlayout)
    DrawerLayout drawerLayout;

    Fragment labFragment, ingListFragment, staffFragment, ingDetailsFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);

        ActiveAndroid.initialize(this);

        labFragment = new LabFragment();
        ingListFragment = new IngListFragment();
        staffFragment = new StaffFragment();
        ingDetailsFragment = new IngDetailsFragment();

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.activity_main_container, labFragment)
                .commit();

        ArrayList<HashMap<String, String>> menuList = new ArrayList<>();
        HashMap<String, String> labEntry = new HashMap<>();
        labEntry.put("icon", Integer.toString(R.drawable.ic_launcher));
        labEntry.put("label", getResources().getString(R.string.lab));
        menuList.add(labEntry);

        HashMap<String, String> staffEntry = new HashMap<>();
        staffEntry.put("icon", Integer.toString(R.drawable.ic_launcher));
        staffEntry.put("label", getResources().getString(R.string.staff));
        menuList.add(staffEntry);

        HashMap<String, String> ingEntry = new HashMap<>();
        ingEntry.put("icon", Integer.toString(R.drawable.ic_launcher));
        ingEntry.put("label", getResources().getString(R.string.ing));
        menuList.add(ingEntry);

        String from[] = {"icon", "label"};
        int to[] = {R.id.list_item_navigation_drawer_imageview_icon,
                R.id.list_item_navigation_drawer_textview_label};

        navigationDrawerListView.setAdapter(new SimpleAdapter(getBaseContext(), menuList,
                R.layout.list_item_navigation_drawer, from, to));

        navigationDrawerListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position)
                {
                    case 0:
                        getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.activity_main_container, labFragment)
                                .commit();
                        break;
                    case 1:
                        getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.activity_main_container, staffFragment)
                                .commit();
                        break;
                    case 2:
                        getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.activity_main_container, ingListFragment)
                                .commit();
                        break;
                }
                drawerLayout.closeDrawers();
            }
        });

//        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
//        actionBar.setDisplayHomeAsUpEnabled(true);
//        actionBar.setHomeButtonEnabled(true);

//        final android.support.v4.app.ActionBarDrawerToggle actionBarDrawerToggle =
//                new android.support.v4.app.ActionBarDrawerToggle(this, drawerLayout,
//                        R.drawable.ic_action_next_item, R.string.hello_world, R.string.hello_world) {
//                    @Override
//                    public void onDrawerClosed(View drawerView) {
//                        invalidateOptionsMenu(); // calls onPrepareOptionsMenu()
//                    }
//
//                    @Override
//                    public void onDrawerOpened(View drawerView) {
//                        invalidateOptionsMenu(); // calls onPrepareOptionsMenu()
//                    }
//                };
//
//        drawerLayout.post(new Runnable() {
//            @Override
//            public void run() {
//                actionBarDrawerToggle.syncState();
//            }
//        });
//
//        drawerLayout.setDrawerListener(actionBarDrawerToggle);

    }

    public void setActionBarTitle(String title)
    {
        getSupportActionBar().setTitle(title);
    }

    @Override
    protected void onResume() {
        super.onResume();
        App.getBus().getDefault().register(this);
    }

    public void onEventAsync(GotoIngButtonClickedEvent event)
    {
         getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.activity_main_container, ingListFragment)
                .commit();
    }

    public void onEventMainThread(IngListItemClickedEvent event)
    {
        Bundle bundle = new Bundle();
        bundle.putLong("ingId", event.getId());
        ingDetailsFragment.setArguments(bundle);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.activity_main_container, ingDetailsFragment)
                .commit();
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }
}
