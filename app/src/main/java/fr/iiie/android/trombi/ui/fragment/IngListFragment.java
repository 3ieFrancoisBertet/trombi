package fr.iiie.android.trombi.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import fr.iiie.android.trombi.R;
import fr.iiie.android.trombi.adapter.IngAdapter;
import fr.iiie.android.trombi.bus.IngListItemAnimFinishedEvent;
import fr.iiie.android.trombi.bus.IngListItemClickedEvent;
import fr.iiie.android.trombi.bus.IngListUpdateEvent;
import fr.iiie.android.trombi.bus.IngListUpdateFinishedEvent;
import fr.iiie.android.trombi.model.Ing;
import fr.iiie.android.trombi.ui.activity.MainActivity;
import fr.iiie.android.trombi.ui.view.RotatingImageView;

/**
 * Created by francois.bertet on 2/5/2015.
 */
public class IngListFragment extends Fragment {

    @InjectView(R.id.fragment_ing_list_listview)
    ListView ingListView;

    @InjectView(R.id.fragment_ing_list_swipe_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    IngAdapter ingListAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ((MainActivity) getActivity()).setActionBarTitle(getResources().getString(R.string.ing));
        View fragmentView = inflater.inflate(R.layout.fragment_ing_list, container, false);
        ButterKnife.inject(this, fragmentView);

        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_red_light, android.R.color.holo_green_light);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                EventBus.getDefault().post(new IngListUpdateEvent());
            }
        });

        ingListAdapter = new IngAdapter(getActivity());
        ingListView.setAdapter(ingListAdapter);

        ingListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ((RotatingImageView)view.findViewById(R.id.list_item_ing_imageview_arrow)).rotate(position);
            }
        });

        ingListAdapter.updateList(Ing.getIngs());

        return fragmentView;
    }

    public void onEventAsync(IngListUpdateEvent event)
    {
        Ing.addIng(new Ing("Test", "Test", 20, "Android", "NoSpe", "francois.bertet@epita.fr"));
        try
        {
            Thread.sleep(2000);
        } catch (InterruptedException e)
        {
            e.printStackTrace();
        }
        EventBus.getDefault().post(new IngListUpdateFinishedEvent(Ing.getIngs()));
    }

    public void onEventMainThread(IngListItemAnimFinishedEvent event)
    {
        Crouton.makeText(getActivity(), "Let's Go", Style.INFO).show();
        EventBus.getDefault().post(new IngListItemClickedEvent(ingListAdapter.getItem((event.getPosition())).getId()));
    }

    public void onEventMainThread(IngListUpdateFinishedEvent event)
    {
        ingListAdapter.updateList(event.getIngList());
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onResume()
    {
        EventBus.getDefault().register(this);
        super.onResume();
    }

    @Override
    public void onPause()
    {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Override
    public void onDestroy()
    {
        Crouton.cancelAllCroutons();
        super.onDestroy();
    }
}
