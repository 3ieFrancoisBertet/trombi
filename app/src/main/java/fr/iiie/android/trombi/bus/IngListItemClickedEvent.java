package fr.iiie.android.trombi.bus;

/**
 * Created by francois.bertet on 2/5/2015.
 */
public class IngListItemClickedEvent
{
    private long id;

    public IngListItemClickedEvent(long id)
    {
        this.id = id;
    }

    public long getId()
    {
        return id;
    }
}