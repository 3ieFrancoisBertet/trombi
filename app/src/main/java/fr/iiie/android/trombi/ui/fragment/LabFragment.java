package fr.iiie.android.trombi.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import fr.iiie.android.trombi.R;
import fr.iiie.android.trombi.bus.GotoIngButtonClickedEvent;
import fr.iiie.android.trombi.ui.activity.MainActivity;

/**
 * Created by francois.bertet on 2/5/2015.
 */
public class LabFragment extends Fragment {


    @OnClick(R.id.fragment_lab_imageview_image)
    protected void onGotoIng1ButtonClicked()
    {
        EventBus.getDefault().post(new GotoIngButtonClickedEvent(5));
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ((MainActivity) getActivity()).setActionBarTitle(getResources().getString(R.string.lab));
        View fragmentView = inflater.inflate(R.layout.fragment_lab, container, false);
        ButterKnife.inject(this, fragmentView);
        return fragmentView;
    }
}
