package fr.iiie.android.trombi.rest;

import fr.iiie.android.trombi.rest.service.StaffApi;
import retrofit.RestAdapter;

/**
 * Created by francois.bertet on 2/5/2015.
 */
public class RestClient {
    private static final String BASE_URL = "http://education.3ie.fr";
    private StaffApi staffApi;

    public RestClient()
    {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(BASE_URL)
                .build();

        staffApi = restAdapter.create(StaffApi.class);
    }

    public StaffApi getStaffApi()
    {
        return staffApi;
    }
}
