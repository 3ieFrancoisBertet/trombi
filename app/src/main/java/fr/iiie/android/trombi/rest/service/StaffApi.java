package fr.iiie.android.trombi.rest.service;

import java.util.ArrayList;

import fr.iiie.android.trombi.rest.model.Staff;
import retrofit.Callback;
import retrofit.http.GET;

public interface StaffApi {
    @GET("/android/ing1/staff3ie.json")
    public void getStaff(Callback<ArrayList<Staff>> callback);
}
