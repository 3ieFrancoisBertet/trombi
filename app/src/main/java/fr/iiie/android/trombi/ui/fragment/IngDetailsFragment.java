package fr.iiie.android.trombi.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import fr.iiie.android.trombi.R;
import fr.iiie.android.trombi.model.Ing;
import fr.iiie.android.trombi.ui.activity.MainActivity;

/**
 * Created by francois.bertet on 2/5/2015.
 */
public class IngDetailsFragment extends Fragment {

    @InjectView(R.id.fragment_ing_details_textview_firstname)
    TextView firstnameTextView;
    @InjectView(R.id.fragment_ing_details_textview_lastname)
    TextView lastnameTextView;
    @InjectView(R.id.fragment_ing_details_textview_age)
    TextView ageTextView;
    @InjectView(R.id.fragment_ing_details_textview_technology)
    TextView technologyTextView;
    @InjectView(R.id.fragment_ing_details_textview_specialization)
    TextView specializationTextView;
    @InjectView(R.id.fragment_ing_details_textview_hobbies)
    TextView hobbiesTextView;

    Ing currentIng;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        long id = getArguments().getLong("ingId");
        currentIng = Ing.getIng(id);

        Log.d("Test ", "" + getArguments().getLong("ingId"));
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ((MainActivity) getActivity()).setActionBarTitle(currentIng.getFirstName() + " " + currentIng.getLastName());
        View fragmentView = inflater.inflate(R.layout.fragment_ing_details, container, false);
        ButterKnife.inject(this, fragmentView);

        firstnameTextView.setText(currentIng.getFirstName());
        lastnameTextView.setText(currentIng.getLastName());
        ageTextView.setText(Integer.toString(currentIng.getAge()));
        technologyTextView.setText(currentIng.getTechnology());
        specializationTextView.setText(currentIng.getSpecialization());
//      hobbiesTextView.setText(currentIng.getHobbies());

        return fragmentView;
    }

    @OnClick(R.id.fragment_ing_details_button_sendmail)
    protected void send_mail()
    {

    }
}
