package fr.iiie.android.trombi.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

@Table(name = "ings")
public class Ing extends Model
{
    @Column(name = "firstname")
    private String strFirstName;

    @Column(name = "lasttname")
    private String strLastName;

    @Column(name = "age")
    private int age;

    @Column(name = "technology")
    private String strTechnology;

    @Column(name = "specialization")
    private String strSpecialization;

    @Column(name = "mail")
    private String strMail;

    public Ing()
    {
        super();
    }

    public Ing(String strFirstName, String strLastName, int age, String strTechnology, String strSpecialization, String strMail)
    {
        super();
        this.strFirstName = strFirstName;
        this.strLastName = strLastName;
        this.age = age;
        this.strTechnology = strTechnology;
        this.strSpecialization = strSpecialization;
        this.strMail = strMail;
    }

    /* Getters */
    public String getFirstName()
    {
        return strFirstName;
    }

    public String getLastName()
    {
        return strLastName;
    }

    public int getAge()
    {
        return age;
    }

    public String getTechnology()
    {
        return strTechnology;
    }

    public String getSpecialization()
    {
        return strSpecialization;
    }

    public String getMail()
    {
        return strMail;
    }

    /* Requests */
    public static void addIng(Ing ing)
    {
        ing.save();
    }

    public static Ing getIng(long id)
    {
        return new Select().from(Ing.class).where("id = ?", id).executeSingle();
    }

    public static List<Ing> getIngs()
    {
        return new Select().from(Ing.class).execute();
    }
}
