package fr.iiie.android.trombi.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import fr.iiie.android.trombi.R;
import fr.iiie.android.trombi.model.Ing;

/**
 * Created by francois.bertet on 2/5/2015.
 */
public class IngAdapter extends BaseAdapter {

    List<Ing> dataList;
    LayoutInflater layoutInflater;

    public IngAdapter(Context context) {
        this.layoutInflater = LayoutInflater.from(context);
        this.dataList = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Ing getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView;
        ViewHolder viewHolder;
        Ing ing = getItem(position);

        if (convertView == null)
        {
            rowView = layoutInflater.inflate(R.layout.list_item_ing, parent, false);
            viewHolder = new ViewHolder(rowView);
            rowView.setTag(viewHolder);
        }
        else
        {
            rowView = convertView;
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.labelTextView.setText(ing.getFirstName() + " " + ing.getLastName());

        return rowView;
    }

    class ViewHolder
    {
        @InjectView(R.id.list_item_ing_textview_label)
        TextView labelTextView;

        ViewHolder(View v) {
            ButterKnife.inject(this, v);
        }
    }

    public void addAToList(List<Ing> ingArrayList)
    {
        dataList.addAll(ingArrayList);
        this.notifyDataSetChanged();
    }

    public void updateList(List<Ing> ingArrayList)
    {
        dataList.clear();
        addAToList(ingArrayList);
    }
}
