package fr.iiie.android.trombi.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ProgressBar;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import fr.iiie.android.trombi.R;
import fr.iiie.android.trombi.adapter.StaffAdapter;
import fr.iiie.android.trombi.app.App;
import fr.iiie.android.trombi.rest.model.Staff;
import fr.iiie.android.trombi.ui.activity.MainActivity;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by francois.bertet on 2/5/2015.
 */
public class StaffFragment extends Fragment {

    @InjectView(R.id.fragment_staff_gridview)
    GridView staffGridView;

    @InjectView(R.id.fragment_staff_loader)
    ProgressBar loader;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        ((MainActivity) getActivity()).setActionBarTitle(getResources().getString(R.string.staff));
        View fragmentView = inflater.inflate(R.layout.fragment_staff, container, false);
        ButterKnife.inject(this, fragmentView);

        final StaffAdapter adapter = new StaffAdapter(getActivity());
        staffGridView.setAdapter(adapter);

        displayLoader(true);
        App.getRestClient().getStaffApi().getStaff(new Callback<ArrayList<Staff>>()
        {
            @Override
            public void success(ArrayList<Staff> staffList, Response response)
            {
                adapter.updateList(staffList);
                displayLoader(false);
            }

            @Override
            public void failure(RetrofitError error) {
            }
        });

        return fragmentView;
    }

    private void displayLoader(boolean bool)
    {
        loader.setVisibility(bool ? View.VISIBLE : View.GONE);
    }
}
