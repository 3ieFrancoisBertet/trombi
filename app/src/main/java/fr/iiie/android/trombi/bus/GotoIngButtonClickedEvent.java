package fr.iiie.android.trombi.bus;

/**
 * Created by francois.bertet on 2/5/2015.
 */
public class GotoIngButtonClickedEvent {
    private Integer counter;

    public GotoIngButtonClickedEvent(Integer counter) {
        this.counter = counter;
    }

    public Integer getCounter() {

        return counter;
    }
}
