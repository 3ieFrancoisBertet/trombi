package fr.iiie.android.trombi.app;

import android.app.Application;

import de.greenrobot.event.EventBus;
import fr.iiie.android.trombi.rest.RestClient;

/**
 * Created by francois.bertet on 2/5/2015.
 */
public class App extends Application {

    private static EventBus bus;
    private static RestClient restClient;

    // Useless mais exemple de comment on procède en utilisant Otto
    public static EventBus getBus()
    {
        if (bus == null)
            bus = new EventBus();
        return bus;
    }

    public static RestClient getRestClient()
    {
        if (restClient == null)
            restClient = new RestClient();
        return restClient;
    }
}
