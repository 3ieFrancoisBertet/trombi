package fr.iiie.android.trombi.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import de.greenrobot.event.EventBus;
import fr.iiie.android.trombi.R;
import fr.iiie.android.trombi.bus.IngListItemAnimFinishedEvent;

/**
 * Created by francois.bertet on 2/6/2015.
 */
public class RotatingImageView extends ImageView
{


    public RotatingImageView(Context context)
    {
        super(context);
    }

    public RotatingImageView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public RotatingImageView(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
    }

    public void rotate(int position)
    {
        Animation rotation = AnimationUtils.loadAnimation(getContext(), R.anim.rotation);
        rotation.setAnimationListener(new AnimationListenerWithPos(position));
        this.startAnimation(rotation);
    }

    class AnimationListenerWithPos implements Animation.AnimationListener
    {
        int position;

        AnimationListenerWithPos(int position)
        {
            this.position = position;
        }

        @Override
        public void onAnimationStart(Animation animation)
        {

        }

        @Override
        public void onAnimationEnd(Animation animation)
        {
            EventBus.getDefault().post(new IngListItemAnimFinishedEvent(position));
        }

        @Override
        public void onAnimationRepeat(Animation animation)
        {

        }
    };
}
