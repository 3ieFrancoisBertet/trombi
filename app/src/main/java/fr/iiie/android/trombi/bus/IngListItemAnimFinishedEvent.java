package fr.iiie.android.trombi.bus;

/**
 * Created by francois.bertet on 2/5/2015.
 */
public class IngListItemAnimFinishedEvent
{
    private int position;

    public IngListItemAnimFinishedEvent(int position) {
        this.position = position;
    }

    public Integer getPosition() {

        return position;
    }
}