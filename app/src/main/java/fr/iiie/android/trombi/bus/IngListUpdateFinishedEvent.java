package fr.iiie.android.trombi.bus;

import java.util.List;

import fr.iiie.android.trombi.model.Ing;

public class IngListUpdateFinishedEvent
{
    private List<Ing> ingList;

    public IngListUpdateFinishedEvent(List<Ing> ingList)
    {
        this.ingList = ingList;
    }

    public List<Ing> getIngList()
    {
        return ingList;
    }
}
